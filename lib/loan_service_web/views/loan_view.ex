defmodule LoanServiceWeb.LoanView do
  use LoanServiceWeb, :view

  alias Ecto.Changeset

  def get_key(changeset, key), do: Changeset.get_change(changeset, key)
end
