defmodule LoanServiceWeb.AdminView do
  use LoanServiceWeb, :view

  def get_application_result(application, interests) do
    id = application.id

    interest =
      interests
      |> Enum.filter(fn interest -> interest.loan_application_id == id end)
      |> List.first()

    case interest do
      nil -> "REJ"
      interest -> interest.rate
    end
  end
end
