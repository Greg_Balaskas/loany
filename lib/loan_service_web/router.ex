defmodule LoanServiceWeb.Router do
  use LoanServiceWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", LoanServiceWeb do
    pipe_through :browser

    get "/", LoanController, :new

    post "/", LoanController, :create
  end

  scope "/admin", LoanServiceWeb do
    pipe_through :browser

    get "/", AdminController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", LoanServiceWeb do
  #   pipe_through :api
  # end
end
