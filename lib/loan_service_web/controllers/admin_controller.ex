defmodule LoanServiceWeb.AdminController do
  use LoanServiceWeb, :controller
  alias LoanService.{LoanApplication, LoanInterest, Repo}

  def index(conn, _params) do
    applications = Repo.all(LoanApplication)
    interests = Repo.all(LoanInterest)
    render(conn, "index.html", applications: applications, interests: interests)
  end
end
