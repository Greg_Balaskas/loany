defmodule LoanServiceWeb.LoanController do
  use LoanServiceWeb, :controller

  require Logger

  alias Ecto.Changeset
  alias LoanService.{Engine, LoanApplication, LoanInterest, Repo}

  def new(conn, _params) do
    loan_application = LoanApplication.changeset(%LoanApplication{}, %{})
    render(conn, "new.html", changeset: loan_application)
  end

  def create(conn, params) do
    loan_application = Map.get(params, "loan_application")
    changeset = LoanApplication.changeset(%LoanApplication{}, loan_application)
    Logger.debug(inspect(changeset))

    with true <- changeset.valid?,
         {:ok, amount} <- Changeset.fetch_change(changeset, :amount),
         {:ok, %{id: loan_application_id}} <- Repo.insert(changeset),
         {:ok, interest_rate} <- Engine.calculate_interest_rate(amount),
         {:ok, _} <- insert_interest_rate(interest_rate, loan_application_id) do
      render(conn, "offer.html", changeset: changeset, interest_rate: interest_rate)
    else
      false ->
        conn
        |> put_flash(:error, "Invalid Data")
        |> render("new.html", changeset: changeset)

      {:error, :rejected} ->
        render(conn, "rejection.html")
    end
  end

  defp insert_interest_rate(interest_rate, loan_application_id) do
    changeset =
      LoanInterest.changeset(
        %LoanInterest{},
        %{loan_application_id: loan_application_id, rate: interest_rate}
      )

    Logger.debug(inspect(changeset))

    Repo.insert(changeset)
  end
end
