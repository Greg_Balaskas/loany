defmodule LoanServiceWeb.PageController do
  use LoanServiceWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
