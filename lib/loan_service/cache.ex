defmodule LoanService.Cache do
  @moduledoc false

  @module __MODULE__

  use GenServer

  import Ecto.Query

  alias LoanService.{LoanApplication, Repo}

  require Logger

  def start_link(_) do
    GenServer.start_link(@module, nil)
  end

  def get_minimum_amount(), do: :ets.lookup(:cache, :minimum_amount)[:minimum_amount]

  def update_minimum_amount(amount), do: :ets.insert(:cache, {:minimum_amount, amount})

  @impl true
  def init(_init_arg) do
    ets = :ets.new(:cache, [:named_table, :set, :public])

    amounts = Repo.all(from application in LoanApplication, select: application.amount)
    minimum_amount = Enum.min(amounts, fn -> 0 end)

    :ets.insert(ets, {:minimum_amount, minimum_amount})
    Logger.info("Cache initialized: #{minimum_amount}")
    {:ok, ets}
  end
end
