defmodule LoanService.Engine do
  @moduledoc false

  alias LoanService.Cache

  def calculate_interest_rate(amount) do
    current_minimum = Cache.get_minimum_amount()

    cond do
      amount < current_minimum ->
        Cache.update_minimum_amount(amount)
        {:error, :rejected}

      prime?(amount) ->
        {:ok, 9.99}

      true ->
        {:ok, generate_random_interest(4, 12)}
    end
  end

  defp prime?(2), do: true

  defp prime?(num) do
    last =
      num
      |> :math.sqrt()
      |> Float.ceil()
      |> trunc

    notprime =
      2..last
      |> Enum.any?(fn a -> rem(num, a) == 0 end)

    !notprime
  end

  defp generate_random_interest(from, to) do
    rand_int = Enum.random((from * 100)..(to * 100))
    rand_int / 100
  end
end
