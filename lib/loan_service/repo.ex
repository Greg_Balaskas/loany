defmodule LoanService.Repo do
  use Ecto.Repo,
    otp_app: :loan_service,
    adapter: Ecto.Adapters.Postgres
end
