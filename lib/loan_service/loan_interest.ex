defmodule LoanService.LoanInterest do
  use Ecto.Schema
  import Ecto.Changeset

  schema "loan_interests" do
    field :rate, :float
    field :loan_application_id, :id

    timestamps()
  end

  @doc false
  def changeset(loan_interest, attrs) do
    loan_interest
    |> cast(attrs, [:rate, :loan_application_id])
    |> validate_required([:rate, :loan_application_id])
  end
end
