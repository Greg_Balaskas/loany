defmodule LoanService.LoanApplication do
  use Ecto.Schema
  import Ecto.Changeset

  schema "loan_applications" do
    field :amount, :integer
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :phone, :string

    timestamps()
  end

  @doc false
  def changeset(loan_application, attrs) do
    loan_application
    |> cast(attrs, [:amount, :first_name, :last_name, :phone, :email])
    |> validate_required([:amount, :first_name, :last_name, :phone, :email])
    |> validate_format(:email, ~r/@/)
    |> validate_number(:amount, greater_than: 0)
    |> validate_format(:phone, ~r/^[[:digit:]]+$/)
  end
end
