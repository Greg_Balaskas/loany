# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :loan_service,
  ecto_repos: [LoanService.Repo]

# Configures the endpoint
config :loan_service, LoanServiceWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "FHTJnDWusoQLpDHpK7sReeu5KxtL8wdJ4uR5dnSJ/Z2fTAXIK9zE0PWpXFp072g3",
  render_errors: [view: LoanServiceWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: LoanService.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
