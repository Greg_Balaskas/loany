defmodule LoanService.Repo.Migrations.CreateLoanApplications do
  use Ecto.Migration

  def change do
    create table(:loan_applications) do
      add :amount, :integer
      add :first_name, :string
      add :last_name, :string
      add :phone, :string
      add :email, :string

      timestamps()
    end

  end
end
