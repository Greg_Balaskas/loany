defmodule LoanService.Repo.Migrations.CreateLoanInterests do
  use Ecto.Migration

  def change do
    create table(:loan_interests) do
      add :rate, :float
      add :loan_application_id, references(:loan_applications, on_delete: :nothing)

      timestamps()
    end

    create index(:loan_interests, [:loan_application_id])
  end
end
